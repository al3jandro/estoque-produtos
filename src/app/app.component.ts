import { ProductsService } from './shared/services/products.service';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'ibureau';
  produtos: any;
  mercado: any;

  constructor(
    private api: ProductsService,
    private spinner: NgxSpinnerService
   ) { }

  ngOnInit() {
    this.getProdutos();
  }

  getProdutos() {
    this.api.getAll().subscribe(
      data => {
        this.produtos = data;
    }, err => {
      console.log(err);
    });
  }

  onMercado(id: number) {
    this.api.getById(id).subscribe(
     data => {
       this.loader();
       this.mercado = data;
     }, err => {
       console.log(err);
     }
    );
  }

  private loader() {
    this.spinner.show();
    setTimeout(() => { 
      this.spinner.hide();
    }, 1500);
  }

  ordenarDesc(arr, key) {
    this.ordenarAsc(arr, key); 
    arr.reverse();
  }

  ordenarAsc(arr, key) {
    arr.sort(function (a, b) {
       return a[key] > b[key];
    });
  }
}
