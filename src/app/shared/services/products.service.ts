import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  public url = 'https://ibureau.herokuapp.com';
 
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(`${this.url}/products`);
  }

  getById(id: number) {
    return this.http.get(`${this.url}/products/${id}`);
  }
}
